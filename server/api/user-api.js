const express = require('express');
const router = express.Router();
const KeycloakClient = require('../utils/keycloak-client');
const User = require('../model/user');

router.get('/', async (req, res) => {
    const users = await KeycloakClient.getUsers();
    return res.status(200).send(users);
});

router.post('/', async (req, res) => {
    let user = User.from(req.body);
    let properties = User.required();
    let property = properties.find(p => user[p] === undefined);
    if (property !== undefined) {
        return res.status(400).send({message: `Missing property ${property}`});
    }

    try {
        user = await KeycloakClient.createUser(user);
    } catch (e) {
        if (e.response?.data?.errorMessage === 'User exists with same username'){
            return res.status(409).send({message: 'User exists with same username'});
        }
        return res.status(500).send({message: e.message});
    }

    return res.status(200).send(user);
});


module.exports = router;
