require('dotenv').config();
const session = require('express-session'),
    Keycloak = require('keycloak-connect'),
    express = require('express'),
    cors = require('cors');

const users = require('./api/user-api');

const port = process.env.PORT,
    keycloakUrl = process.env.KEYCLOAK_URL,
    keycloakSecret = process.env.KEYCLOAK_SECRET;

const kcConfig = {
    'realm': 'iam',
    'bearer-only': true,
    'auth-server-url': keycloakUrl,
    'clientId': 'be'
};
const memoryStore = new session.MemoryStore();
const keycloak = new Keycloak({store: memoryStore}, kcConfig);
const app = express();

app.use(session({
    secret: keycloakSecret,
    resave: false,
    saveUninitialized: true,
    store: memoryStore
}));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(keycloak.middleware());

app.get('/', (req, res) => res.send('Hello World!'));
app.use('/users', keycloak.protect(), users);

app.use((req, res) => res.status(404).send('API not found'));
app.use((err, req, res) => {
    console.error(err);
    res.status(err.status || 500).send('Internal Server Error');
});


app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`));
