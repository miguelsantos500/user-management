module.exports = class User {
    id;
    username;
    firstName;
    lastName;
    email;
    createdAt;
    tempPassword;

    constructor(id, username, firstName, lastName, email, createdAtTimeStamp) {
        this.id = id;
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.createdAt = new Date(createdAtTimeStamp);
    }

    static from(json) {
        let user = Object.assign(new User(), json);
        if (user.email === undefined) {
            user.username = user.email;
        }
        return user;
    }

    static required() {
        return ['username', 'firstName', 'lastName']
    }
};
