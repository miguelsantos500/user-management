const axios = require('axios');
const url = require('url');

const User = require('../model/user');
const {v4} = require("uuid");

const auth = {
    url: `${process.env.KEYCLOAK_URL}realms/master/protocol/openid-connect/token`,
    body: new url.URLSearchParams({
        client_id: 'admin',
        client_secret: process.env.KEYCLOAK_ADMIN_SECRET,
        grant_type: 'client_credentials'
    }).toString(),
    config: {
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }
};

class KeycloakClient {
    /** @type {{token: String, expiration: Date}} */
    static token;

    constructor() {
        throw new Error("Should not be initialized");
    }

    /**
     * @returns {Promise<>}
     */
    static async checkToken() {
        if (KeycloakClient.token?.expiration.getTime() > new Date().getTime()) {
            return;
        }
        return axios.post(auth.url, auth.body, auth.config).then(res => KeycloakClient.token = {
            token: res.data.access_token,
            expiration: new Date(new Date().getTime() + (1000 * (res.data.expires_in - 1)))
        });
    }

    /**
     * @returns {Promise<User>}
     */
    static async getUsers() {
        await KeycloakClient.checkToken();
        return axios.get(`${process.env.KEYCLOAK_URL}admin/realms/iam/users`, {
            params: {briefRepresentation: true},
            headers: {Authorization: `Bearer ${KeycloakClient.token.token}`}
        }).then(res => res.data.map(u =>
            new User(u.id, u.username, u.firstName, u.lastName, u.email, u.createdTimestamp)));
    }

    /**
     * @returns {Promise<User>}
     */
    static async createUser(user) {
        await KeycloakClient.checkToken();
        let url = `${process.env.KEYCLOAK_URL}admin/realms/iam/users`;
        return axios.post(url, {
            username: user.username,
            firstName: user.firstName,
            lastName: user.lastName,
            email: user.email,
            enabled: true
        }, {
            headers: {
                Authorization: `Bearer ${KeycloakClient.token.token}`,
                'Content-Type': 'application/json'
            }
        }).then((res) => {
            user.id = res.headers.location.replace(`${url}/`, '');
            return KeycloakClient.resetPassword(user);
        }).then((created) => created);
    }
    /**
     * @returns {Promise<User>}
     */
    static async resetPassword(user) {
        user.tempPassword = v4();
        await KeycloakClient.checkToken();
        return axios.put(`${process.env.KEYCLOAK_URL}admin/realms/iam/users/${user.id}/reset-password`, {
            temporary: true,
            value: user.tempPassword
        }, {
            headers: {
                Authorization: `Bearer ${KeycloakClient.token.token}`,
                'Content-Type': 'application/json'
            }
        }).then(() => user);
    }
}

module.exports = KeycloakClient;
