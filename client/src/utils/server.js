const URI = 'http://localhost:3000';

export class Server {
    /** @type {Keycloak.KeycloakInstance} */
    keycloak;

    constructor(keycloak) {
        this.keycloak = keycloak;
    }

    getUsers() {
        return fetch(`${URI}/users/`,
            {
                headers: {
                    Authorization: 'Bearer ' + this.keycloak.token
                }
            }).then(response => response.json());
    }

    logout() {
        return this.keycloak.logout();
    }

    createUser(email, firstName, lastName) {
        return fetch(`${URI}/users/`,
            {
                method: 'POST',
                body: JSON.stringify({username: email, email, firstName, lastName}),
                headers: {
                    Authorization: 'Bearer ' + this.keycloak.token,
                    'Content-Type': 'application/json',
                }
            }).then(response => {
            if (response.status !== 200) {
                throw response;
            }
            return response.json();
        });
    }
}
