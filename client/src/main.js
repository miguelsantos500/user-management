import {createApp} from 'vue';
import App from './App.vue';
import './registerServiceWorker';
import router from './router';
import store from './store';
import {Server} from './utils/server';
import Keycloak from 'keycloak-js';
import {bus} from './utils/bus';


import './assets/css/main.css';

let initOptions = {
    url: 'http://localhost:7001/auth',
    realm: 'iam',
    clientId: 'fe',
    onLoad: 'login-required'
};


const keycloak = Keycloak(initOptions);

keycloak.init({onLoad: initOptions.onLoad}).then((auth) => {
    if (!auth) {
        window.location.reload();
        return;
    }
    let app = createApp(App, {server: new Server(keycloak)});
    app.config.globalProperties.$bus = bus;
    app
        .use(store)
        .use(router)
        .mount('#app');

//Token Refresh
    setInterval(() => keycloak.updateToken(70)
        .catch(() => console.error('Failed to refresh token')), 6000);

});




